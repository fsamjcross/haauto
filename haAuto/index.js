const dotenv = require('dotenv')
const Homeassistant = require('node-homeassistant')
var flatten = require('flat')
var requireDir = require('require-dir');
var automations = flatten(requireDir('./automations/',{recurse: true}), { maxDepth: 2 });

dotenv.config();
const config = {
  	host: process.env.HOME_HOST || 'homeassistant.local', 
  	protocol: 'ws', // "ws" (default) or "wss" for SSL
  	retryTimeout: 1000, // in ms, default is 5000
  	retryCount: 3, // default is 10, values < 0 mean unlimited
  	token: process.env.HOME_TOKN ,
  	port: process.env.HOME_PORT || '8123'
}

let ha = new Homeassistant(config)

ha.connect().then(() => {
			for (key in automations){
				load(automations[key])
			}
}).catch((err)=>{
	console.error(err)
})

load = (automation)=>{
	automation.Descrption = automation.Descrption(automation.config)
	console.log(automation.Name +' | ' + automation.Descrption)
	automation.load(ha,automation.config)
}
