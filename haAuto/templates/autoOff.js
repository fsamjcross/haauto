const sleep = require('await-sleep');

const config = {
	Name: `autoOff`,
	Descrption: (config) => `auto swich off the ${config.entity_name} after ${config.time} seconds`,
	config:{
		"time":1,
		"entity_name":"Hall way light",
		"entity_id":"light.hallmain",
	},
	load(ha,config){
		console.log(`state:${config.entity_id}`)
		ha.on(`state:${config.entity_id}`,
				data => this.action({ha,data,config})
		)
	},
	action: async function({ha,data,config}){
		console.log(`fireing ${this.Name}`)
			await sleep(config.time * 1000)
			console.log(config)
			ha.call({
      			"domain": 'light',
      			"service": 'turn_off',
      			"service_data":{
      				"entity_id": config.entity_id
      			}
    		})

		}
}

module.exports = config
